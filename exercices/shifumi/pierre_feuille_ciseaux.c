#include <stdlib.h>
#include <stdio.h>
#include <time.h>

///////////////////////////////////////////////////////////////////////////////
//
//	Fonction :	get_user_choice()
//
//	Rôle :		récupérer le choix utilisateur à l'aide de scanf(), puis return
//				ce choix; En cas de choix erroné, message d'erreur puis exit()
//
///////////////////////////////////////////////////////////////////////////////



int		main(void)
{
	srand(time(NULL));
	int		choix_ordi;
	int		choix_joueur;

	choix_ordi = rand() % 3;		//Stocke aléatoirement 0, 1 ou 2 dans choix ordi
	
	printf("Veuillez entrer 0 pour pierre, 1 pour feuille, 2 pour ciseau : \n");
	choix_joueur = get_user_choice();

	// Ici construire les conditions, par ex : si (choix_ordi == pierre ET choix_joueur == ciseaux )
	//											printf("Pierre gagne ! Vous avez perdu");

	return (0);
}
