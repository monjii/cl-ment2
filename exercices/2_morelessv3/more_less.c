///////////////////////////////////////////////////////////////////////////////
//
//		Réalises un petit jeu qui :
//		- Initialise un nombre aléatoire entre 0 et 99
//		- Tente de faire deviner ce nombre à l’utilisateur en lui indiquant si
//		le nombre à trouver est plus petit ou plus grand que sa proposition.
//
//
//		Entrez votre nombre: 50
//		C’est plus!
//		Entrez votre nombre: 25
//		C’est moins!
//		...
//		Gagné!!!
//
//
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int		main()
{
	srand(time(NULL));
	return (0);
}
